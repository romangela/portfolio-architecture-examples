= Headless eCommerce
Eric D. Schabell @eschabell, Iain Boyle @iainboy
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

Retail is the process of selling consumer goods or services to customers through multiple channels of distribution to
earn a profit. The term electronic commerce (e-commerce) refers to a business model that allows companies and
individuals to buy and sell goods and services over the Internet.

*Use case:* Deploying a container based eCommerce website while moving away from tightly coupled existing eCommerce
platform.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/headless-ecommerce-marketing-slide.png[750,700]
--


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/retail-headless-ecommerce-ld.png[750, 700]
--

== The technology
The following technology was chosen for this solution:

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it[*Red Hat OpenShift*] is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy.
It provides a consistent application platform to manage hybrid cloud, multicloud, and edge deployments.

https://www.redhat.com/en/products/runtimes[*Red Hat OpenShift Runtimes*] helps organizations use the cloud delivery model and simplify continuous delivery of
applications, the cloud-native way. Built on proven open source technologies, it also provides development teams
multiple modernization options to enable a smooth transition to the cloud for existing applications.

https://www.redhat.com/en/products/integration[*Red Hat Integration*] is a comprehensive set of integration and messaging technologies to connect applications and
data across hybrid infrastructures.

https://catalog.redhat.com/software/operators/detail/5ef20efd46bc301a95a1e9a4[*Red Hat AMQ Streams*] is a massively scalable, distributed, and high-performance data streaming platform based on
the Apache Kafka project. It offers a distributed backbone that allows microservices and other applications to share
data with high throughput and low latency.

https://www.redhat.com/en/technologies/jboss-middleware/3scale[*Red Hat 3scale API Management*] makes it easy to manage your APIs. Share, secure, distribute, control, and monetize
your APIs on an infrastructure platform built for performance, customer control, and future growth.

https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation[*Red Hat OpenShift Data Foundations*] is software-defined storage for containers. Engineered as the data and storage
services platform for Red Hat OpenShift, Red Hat OpenShift Data Foundation helps teams develop and deploy applications
quickly and efficiently across clouds.

https://www.redhat.com/en/technologies/storage/ceph[*Red Hat Ceph Storage*] is an open, massively scalable, simplified storage solution for modern data pipelines.
Engineered for data analytics, artificial intelligence/machine learning (AI/ML), and emerging workloads, it delivers
software-defined storage on your choice of industry-standard hardware.

https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux[*Red Hat Enterprise Linux*] is the world’s leading enterprise Linux platform. It’s an open source operating system
(OS). It’s the foundation from which you can scale existing apps—and roll out emerging technologies—across bare-metal,
virtual, container, and all types of cloud environments.

== Headless E-commerce local development
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/retail-headless-ecommerce-local-sd.png[750, 700]
--

The developer is central to delivering the components used in this architecture. The developer IDE is the tooling the developer codes in. A source code management repository is used in some form. For this, a Git-based repository was used. Integration is based on Camel, Fuse, and often a collection of Java runtimes. All of the work is done using local container tooling and triggers builds with git hooks, maven plugins, or file uploads.

A container platform hosting the continuous integration (CI) and continuous development (CD) tooling is the main
element in the dev infrastructure. The SCM repository represents the connection between developer and collecting project artifacts for use in the container CI/CD platform for testing ,tagging, and finalizing images for the image management to process out into the test infrastructure.

The test environment is used to roll out the entire headless e-commerce suite of services, web application, and messaging. These are tied to external integration services and the testing web application. The process continues on through more environments until the organization is satisfied to push to production.

== Headless E-commerce remote development
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/retail-headless-ecommerce-remote-sd.png[750, 700]
--

The developer is central to delivering the components used in this architecture. The developer IDE is the tooling the developer codes in. A source code management repository is used in some form. For this, a Git-based repository was used. Integration is based on Camel, Fuse, and often a collection of Java runtimes. All the work is done using remote container tooling and triggers builds with git hooks, maven plugins, or OpenShift client tooling.

A container platform hosting the continuous integration (CI) and continuous development (CD) tooling is the main
element in the dev infrastructure. The SCM repository represents the connection between developer and collecting project artifacts for use in the container CI/CD platform for testing , tagging, and finalizing of images for the image management to process out into the test infrastructure. A source-to-image process is triggered by the remote tooling and the code pulled into a container build process, which is then pushed into the CI/CD testing for eventual tagging.
Once tagged it is placed in the container platform registry which rolls out a dev environment of the headless e-commerce for developer testing. Once satisfied, it's tagged for testing.

The image is pushed to the test environment and rolls out the entire headless e-commerce suite of services, web
application, and messaging. These are tied to external integration services and the testing web application. The
process continues on through more environments until the organization is satisfied to push to production.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/retail-headless-ecommerce.drawio[[Open Diagrams]]
--

== Provide feedback 
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/headlessecommerce.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
